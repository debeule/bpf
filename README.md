# BPF semester project

Learning to work with BPF, a generic kernel execution engine in Linux

I'm going to be discovering how to work with BPF: the various tools associated with it, BPF assembly, ...

# Terminology
* eBPF or BPF: extended BPF or eBPF. A sort of virtual machine that - in response to hooked-on events - executes restricted bytecode in kernel-space. This bytecode can be configured from user space, and is interpreted by a JIT engine that is platform-independent, and imposes restrictions on what can be done to guarantee security and limit perfomance or stability impacts (for example, no unbounded loops or access to certain privileged areas of memory). Can attach to events like disk IO, network IO, kernel functions, ...
* cBPF: classical BPF, as in the "Berkeley Packet Filter". Can filter, truncate or pass on packets, but is much more limited than eBPF. Available on most Unix-like systems, including Linux, \*BSDs, ... [The original paper](https://www.tcpdump.org/papers/bpf-usenix93.pdf) by Steven McCanne and Van Jacobson at Berkeley was written in 1992.
