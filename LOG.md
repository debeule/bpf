## Making a rudimentary load balancer using XDP

I want to make a rudimentary load balancer that basically distributes SYN packets amongst servers, so that these servers may finish the handshake and assume the connection.

Checksums need to be recomputed when packets are changed. This is true at several layers, at least L3 and L4: there are BPF helpers (see `man 7 bpf-helpers`) for these two (`bpf_l3_csum_replace`, `bpf_l4_csum_replace`). However, these work with sk_buffs (not available here, since XDP happens before they are allocated).  
The XDP tutorials were pretty clever about this, and only did things that don't change the checksum, like swapping src and dest MAC or IP addresses. This is regrettable, I think they should mention it at least.

A helper that can help for XDP is the `bpf_csum_diff` which takes raw pointers


## Work with XDP using libbpf and the bpf syscall

After convincing myself that trying to use `bcc` for XDP would be a waste of time since I couldn't find any documentation, I started looking into how others are using XDP.

Eventually I found [xdp-tutorial](https://github.com/xdp-project/xdp-tutorial) from the xdp-project. It goes into quite some detail into how to use XDP. I went through the "basic" lessons and the "packet processing" ones.

This doesn't use `bcc` but rather libbpf directly (see [this article](https://bolinfest.github.io/opensnoop-native/) for some insight in how they relate).  
I was getting used to the comforts provided by `bcc` and its Python bindings, but the tutorials are pretty good and talk about a lot of pitfalls and problems one could encounter.

Below some notes on the process of an XDP program, how it compares to what I saw and did with `bcc`:

* XDP works with pointers to the beginning and end of the raw packet data. You literally have just pointers to the beginning and the end in the `xdp_md` struct. The packet is accessed via DMA.
* there is a concern about endianness since the network order can be different than the endianness of the architecture the program is running on. There are helper functions to convert from network order to host endianness.
* There is a lot of boilerplate that isn't the actual BPF part but things like the loader, userspace utilities etc. Luckily as the lessons progress most of the work focuses on working with the packets.
* some operations require recalculating checksums, which has to be done manually.
* to work with packets, you have to unwrap layer by layer until you get to the one that interests you (so ethernet, IPv4/IPv6, ICMP/UDP/TCP/...)
* the BPF verifier maintains strong guarantees about what addresses are accessed. This means you have to do some checks that might be superfluous just to make the verifier happy, but this is how BPF can maintain security while not giving up performance.
* Different actions can be taken on the packet: it can be dropped (`XDP_DROP`), passed (`XDP_PASS`), retransmitted out on the same interface (`XDP_TX`), transmitted out of another interface (`XDP_REDIRECT`). `XDP_ABORTED` also drops the packet but emits an event to the `xdp:xdp_exception` tracepoint (without overhead if there is nothing listening).


## Moving away from tracing, towards modifying packets

My previous work has focused on what is discussed in Brendan Gregg's book (BPF Performance Tools).  
It is very good for learning to work with tracing tools, doing read-only things to find performance bottlenecks, etc.

However, I now want to actually modify the packets as they come in. I had a suspicion that this is not possible with tracepoints or kprobes, but wasn't sure.  
This is indeed the case: all the access to data using `bcc` is read-only and transparently wrapped using `bpf_probe_read()` calls by `bcc`, at least for any of the hooks documented in Brendan Gregg's book or `bcc`'s documentation.  

I spent some time reading about the various ways to modify packets with BPF. The only ways to do that are XDP (eXpress Data Path) and `tc` (**t**raffic **c**ontrol). These are only mentioned in passing in Brendan Gregg's book, so I spent quite some time getting to know the basics (reading source code, man pages, etc).

XDP is pretty low-level, its hooks happen before the kernel networking stack (which is neat since it reduces attack surface for DoS defence and such, but also not great in a way since you have very raw pointers to work with).  
You can drop packets, modify them, send them out again on the same network interface (and with most high perfomance hardware all of this happens on the NIC itself as I understand it).  
XDP integrates with the existing stack (as opposed to something like DPDK where the stack is implemented in userspace. I found the paper to be a nice read: [The eXpress Data Path: Fast Programmable Packet Processing in the Operating System Kernel](https://github.com/xdp-project/xdp-paper/blob/master/xdp-the-express-data-path.pdf)

`tc` is higher up in the stack and you work with skb structs instead of the xdp pointers to begin and end. I need more background knowledge to understand anything though, I haven't been able to figure out the tc-bpf manpage really.

It seems to be possible to modify things when using `bcc` and XDP, but since that isn't documented anywhere and the only code I found using XDP in `bcc` were a couple of examples in the [examples folder of bcc](https://github.com/iovisor/bcc/blob/master/examples/networking/xdp), I was a bit at a loss as to how to proceed. There were so many things going on there that I didn't really know where to start to make something of my own.


## Week 4-11 March

After the difficulties of getting the names of functions I want to hook a kprobe to, mainly this week I spent time getting an understanding of the Linux kernel and figuring out how to find the function I want to work with. I mainly looked at how SYN packets get sent (in the process to set up TCP connections). I ended up finding the `tcp_output.c` file where there is the `tcp_connect` function, that "builds a SYN and sends it off" ([prototype](https://elixir.bootlin.com/linux/v5.5.6/source/include/net/tcp.h#L446), [ipv4 implementation](https://elixir.bootlin.com/linux/v5.5.6/source/net/ipv4/tcp_output.c#L3565)).

I then set out to use a kprobe to detect when a SYN is sent. The result of this can be seen in [`detect_syn.py`](code/detect_syn.py).  
Interesting things in there are:

* the mapping between the C struct and the python variable `event`
* the `BPF_PERF_OUTPUT()` mechanism, where I sent the C structure to userspace and in the userspace (python) part, I use `perf_buffer_poll` to poll for it.
* the `bpf_ktime_get_ns()` helper function, allowing precise measurement of time within BPF. I also used this to measure the time delta between a kprobe and kretprobe on the `tcp_connect` calls.

While making this SYN detector, I took a look at the data structures available from within BPF ([Maps](https://github.com/iovisor/bcc/blob/master/docs/reference_guide.md#maps)), some notes on those:

* `BPF_HASH` is a hash map (associative array): you save values with a value and a key, and can access or update those later with the key (in another piece of BPF code or in userspace).
* `BPF_ARRAY` is an "int-indexed array which is optimized for fastest lookup and update". An array, as one would have in C. Pre-initialized to 0, no deleting like in the hash map

There are also special structures for making histograms, stack traces, and special arrays to deal with multi-CPU perfomance problems (arrays specific to a CPU, etc).  


## 24 Feb to 4 March

I continued reading Brendan Gregg's book (BPF Performance Tools), it explains a lot about kprobes and why I wasn't getting the examples to work previously (the names aren't stable, since they are not an API that the kernel developers expose explicitly but rather just automatically there).

There are three main ways to do BPF programming: `bpf_trace`, `BCC` and LLVM.  
`bpf_trace` is the highest-level one, and is useful to write one-liners quickly to solve a particular problem (it provides its own higher-level language).  
`BCC` is a compiler allowing you to write C code and compile it into BPF programs (it also has tooling to allow you to write the user-space part of a program in Python).  
According to BPF Performance Tools, users of BPF should not have to ever write LLVM IR, this would be something for developers of BCC or `bpf_trace`. However, I think it can be useful to at least understand roughly what BPF assembly looks like to be able to debug programs written in one of the two front-ends.


## Week of 17-23 February
I did a lot of reading on Linux internals that seem to be required to understand the programs, let alone write some (for example kprobes). I'm not sure if I'm more or less confused now. Probably the former.

Tried out the `http_filter` example: it didn't work at all, I had the same issue as https://github.com/iovisor/bcc/issues/2169. There were a lot of other things that weren't working in the python part, I'm not sure how it was ever working before, maybe it was in older version of python? After fixing some issues in the python code, I got it to work. I'm going to try making a pull request, see what they think of it. (Update, got merged: https://github.com/iovisor/bcc/pull/2767)

I couldn't get to work a lot of other examples either, such as with the "trace" tool where I couldn't attach to a lot of the kprobes that were given as examples. I suspect the names of the kprobes have changed since the examples were written, but I don't know where in the kernel tree this can be found (for instance I was trying to trace usage of chown, which is given as an example with `p::SyS_chown`, but that fails to attach, as does `sys_execve` or `stub_execve`. `do_execve` attaches but doesn't seem to catch any events. I should look at the current implementation of execsnoop, which does work, to see what it does differently. Argh.)

(Note from later: kprobes aren't a stable API, they can be used to trace executions on *one* specific kernel version. For a more stable API, tracepoints should be used)

## 12/02
**Discovery of the tools included with bcc https://github.com/iovisor/bcc/tools**

BPF seems very versatile and useful for debugging of all kinds of system issues.
Existing tools included with bcc are pretty neat and varied.

Some examples: trace exec() syscalls with execsnoop, trace open() syscalls with opensnoop, trace various operations that are slower than a given treshold (for example in nfs, ext4, btrfs etc), summarize garbage collection in programs from languages like java.

Looking into the programs, they don't look *impossibly* complicated. The general structure of the python script is: interpretation of arguments passed to it, and depending on those arguments replace some strings in a BPF program with pieces of code, in order to make the resulting BPF program do what is expected from the arguments.

The BPF program itself is where the interesting stuff happens: this is what gets compiled by bcc into the BPF assembly.  
Some notes about things I found interesting in the BPF programs (at a glance, with the limited things I know about now):  

* `#pragma unroll` before a `for` loop, this is probably not necessary anymore now that BPF supports bounded loops. TODO: check if this is true, maybe try removing one of these pragmas and see what happens.
* to attach the BPF program from within python is pretty cool, you still have to write it in C but can work with the results etc with Python or lua. Once attached, you poll the buffer to work with it.

## 01/02, 10:30 to 13:10
**FOSDEM talks in the [Software Defined Networking devroom](https://fosdem.org/2020/schedule/track/software_defined_networking/).**  
Notes:  

* "Fundamental Technologies We Need to Work on for Cloud-Native Networking": discussion about work that needs to be done in order to have "Cloud-Native Networking", by someone from Intel. Applications in this theoretical system are in containers, have access to raw packets, and can have properties usually connected to "Cloud-Native" applications, like high-availabity, scalability, load-balancing etc. Sounded very interesting although I only got a very high-level grasp about the general idea. Mentions BPF tangentially.
* [Skydive](https://skydive.network): real-time network topology, protocols analysis. Uses BPF for flow probes.
* "Do you really see what’s happening on your NFV infrastructure?": a lot of acronyms. Nothing about BPF, didn't understand much in this talk.
* "Endless Network Programming − An Update from eBPF Land": Update about recent developments around BPF in the Linux kernel. Notably: a change in the limits of the interpreter, like a higher instruction count (from 4k to 1M, and will be bumped up again if someone manages to hit the limit apparently), loops are now allowed (but must be bounded). BTF (BPF Type Format): useful for debugging, contains information for BPF programs and maps. Global data: global variables, global data can be mmap()'ed. BPF trampoline (should really look into all the words and acronyms I don't understand in that slide). Mention of projects that use eBPF by big corps.
* "Cilium: Replacing iptables with eBPF in Kubernetes": Very interesting talk, one of the more typical usages one would think of for BPF. Goes into tail calls, where one can chain multiple BPF programs one after another. Apparently large performance gains compared to iptables. Some Kubernetes-specific jargon I didn't understand much of.
* "Analyzing DPDK applications with eBPF": DPDK (Data Plane Development Kit) is a Linux Foundation project. BSD licensed, which causes some issues when trying to use code from the Linux kernel (which is under GPL). They have an implementation of the BPF "VM". Translation of cBPF into eBPF. DPDK is a collection of libraries for hardware-acceleration on NICs for example.
