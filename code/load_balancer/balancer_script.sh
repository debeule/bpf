# To be used in the "balancer" container
# Use this script to load the xdp program and configure the balancer with a target
# Takes two arguments:
#                       IP and MAC addresses of the target server

mount -t bpf bpf /sys/fs/bpf/
./xdp_loader -d eth0 --force --progsec xdp
./lb_cfg -d eth0 --targets $1@$2
