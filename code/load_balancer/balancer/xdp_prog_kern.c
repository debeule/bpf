/* SPDX-License-Identifier: GPL-2.0 */
#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>
#include <linux/in.h>

#include "../common/parsing_helpers.h"

#define MAX_TARGET_COUNT 64 // max number of target servers for LB

struct __attribute__((packed)) eth_addr {
	unsigned char addr[ETH_ALEN];
};

struct __attribute__((packed)) ipv4_psd_header {
	__u32 src_addr; /* IP address of source host. */
	__u32 dst_addr; /* IP address of destination host. */
	__u8  zero;     /* zero. */
	__u8  proto;    /* L4 protocol type. */
	__u16 len;      /* L4 length. */
};

struct bpf_map_def SEC("maps") targets_map = {
	.type        = BPF_MAP_TYPE_ARRAY,
	.key_size    = sizeof(__u32),
	.value_size  = sizeof(__u32),
	.max_entries = MAX_TARGET_COUNT,
};

struct bpf_map_def SEC("maps") macs_map = {
	.type        = BPF_MAP_TYPE_HASH,
	.key_size    = sizeof(__u32),
	.value_size  = sizeof(struct eth_addr),
	.max_entries = MAX_TARGET_COUNT,
};

static __always_inline __u16 csum_reduce_helper(__u32 csum)
{
	csum = ((csum & 0xffff0000) >> 16) + (csum & 0xffff);
	csum = ((csum & 0xffff0000) >> 16) + (csum & 0xffff);

	return csum;
}

static __always_inline int get_target(__u32 *daddr)
{
	void *target;
	int key = 0;

	target = bpf_map_lookup_elem(&targets_map, &key);
	if (!target)
		return -1;
	__builtin_memcpy(daddr, target, sizeof(__u32));

	return 0;
}

static __always_inline int update_macs(__u32 *dst_ip, struct ethhdr *ethh)
{
	void *target;

	__builtin_memcpy(ethh->h_source, ethh->h_dest, sizeof(struct eth_addr));
	target = bpf_map_lookup_elem(&macs_map, dst_ip);
	if (!target)
		return -1;
	__builtin_memcpy(ethh->h_dest, target, sizeof(struct eth_addr));

	return 0;
}

static __always_inline int handle_tcp(struct xdp_md *ctx, struct ethhdr *ethh,
		struct iphdr *iph, struct tcphdr *tcph)
{

	__u32 size = sizeof(struct iphdr);
	void *data_end = (void *)(long)ctx->data_end;
	struct ipv4_psd_header psdh;
	__u32 csum;

	ethh = (void *)(long)ctx->data;

	if (ethh + 1 > data_end)
		return XDP_ABORTED;

	iph = (struct iphdr *)(ethh+1);
	if (iph + 1 > data_end)
		return XDP_ABORTED;

	/* Change destination address */
	if (get_target(&iph->daddr))
		return XDP_ABORTED;
	if (update_macs(&iph->daddr, ethh))
		return XDP_ABORTED;

	/* Fix IP checksum */
	iph->check = 0;
	iph->check = ~csum_reduce_helper(bpf_csum_diff(0, 0, (__be32 *)iph, size, 0));

	/* TCP HDR */
	tcph = (struct tcphdr *)(iph+1);
	if (tcph + 1 > data_end)
		return XDP_ABORTED;

	/* Fix TCP CSUM */
	tcph->check = 0;
	csum = bpf_csum_diff(0, 0, (__be32 *)tcph,
			sizeof(struct tcphdr), 0);
	
        csum = csum_reduce_helper(csum);
	psdh.src_addr = iph->saddr;
	psdh.dst_addr = iph->daddr;
	psdh.zero = 0;
	psdh.proto = IPPROTO_TCP;
	psdh.len = bpf_htons(bpf_ntohs(iph->tot_len) - sizeof(struct iphdr));
	csum = bpf_csum_diff(0, 0, (__be32*)&psdh, sizeof(struct ipv4_psd_header),
			csum);

	tcph->check = ~csum_reduce_helper(csum);

	return XDP_TX;
}

static __always_inline int handle_udp(struct xdp_md *ctx, struct ethhdr *ethh,
		struct iphdr *iph, struct udphdr *udph)
{
        __u32 size = sizeof(struct iphdr);
	void *data_end = (void *)(long)ctx->data_end;

	ethh = (void *)(long)ctx->data;

	if (ethh + 1 > data_end)
		return XDP_ABORTED;

	iph = (struct iphdr *)(ethh+1);
	if (iph + 1 > data_end)
		return XDP_ABORTED;

	/* Change destination address */
	if (get_target(&iph->daddr))
		return XDP_ABORTED;
	if (update_macs(&iph->daddr, ethh))
		return XDP_ABORTED;

	/* Fix IP checksum */
	iph->check = 0;
	iph->check = ~csum_reduce_helper(bpf_csum_diff(0, 0, (__be32 *)iph, size, 0));

	/* UDP HDR */
	udph = (struct udphdr *)(iph+1);
	if (udph + 1 > data_end)
		return XDP_ABORTED;

	/* "Fix" UDP CSUM */
	udph->check = 0;
	return XDP_TX;
}

SEC("xdp")
int  xdp_prog_simple(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	struct ethhdr *ethh;
	struct iphdr *iph;
	struct tcphdr *tcph;
	struct udphdr *udph;
	__u32 action = XDP_PASS; /* Default action */
	struct hdr_cursor nh;
	int nh_type;
	int ip_type;

	nh.pos = data;

	nh_type = parse_ethhdr(&nh, data_end, &ethh);
	if (nh_type != bpf_htons(ETH_P_IP))
		goto OUT;
	ip_type = parse_iphdr(&nh, data_end, &iph);
	if (ip_type == IPPROTO_TCP){
	if (parse_tcphdr(&nh, data_end, &tcph) < 0) {
		action = XDP_ABORTED;
		goto OUT;
	}
	action = handle_tcp(ctx, ethh, iph, tcph);
        } else if(ip_type == IPPROTO_UDP){
            if (parse_udphdr(&nh, data_end, &udph) < 0) {
	        action = XDP_ABORTED;
		goto OUT;
            }
            action = handle_udp(ctx, ethh, iph, udph);
        }

OUT:
	return action;
}



struct bpf_map_def SEC("maps") xdp_stats_map = {
	.type        = BPF_MAP_TYPE_HASH,
	.key_size    = sizeof(__s32),
	.value_size  = sizeof(__u64),
	.max_entries = 10,
};

struct xdp_exception_ctx {
	__u64 __pad;      // First 8 bytes are not accessible by bpf code
	__s32 prog_id;    //      offset:8;  size:4; signed:1;
	__u32 act;        //      offset:12; size:4; signed:0;
	__s32 ifindex;    //      offset:16; size:4; signed:1;
};

SEC("tracepoint/xdp/xdp_exception")
int trace_xdp_exception(struct xdp_exception_ctx *ctx)
{
	__s32 key = ctx->ifindex;
	__u32 *valp;

	/* Collecting stats only for XDP_ABORTED action. */
	if (ctx->act != XDP_ABORTED)
		return 0;

	/* Lookup in kernel BPF-side returns pointer to actual data. */
	valp = bpf_map_lookup_elem(&xdp_stats_map, &key);

	/* If there's no record for interface, we need to create one,
	 * with number of packets == 1
	 */
	if (!valp) {
		__u64 one = 1;
		return bpf_map_update_elem(&xdp_stats_map, &key, &one, 0) ? 1 : 0;
	}

	(*valp)++;
	return 0;
}


char _license[] SEC("license") = "GPL";
