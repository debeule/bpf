#! /usr/bin/python3
from bcc import BPF
b = BPF(text="""
#include <net/sock.h>
#include <linux/sched.h>
// define output data structure in C
struct data_t {
    u32 pid;
    u64 ts;
    char comm[TASK_COMM_LEN];
};
BPF_PERF_OUTPUT(events);
int kprobe__tcp_connect(struct pt_regs *ctx,
                        struct sock *sk) {
    struct data_t data = {};

    data.pid = bpf_get_current_pid_tgid();
    data.ts = bpf_ktime_get_ns();
    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    events.perf_submit(ctx, &data, sizeof(data));
    return 0;
}
""")
def print_event(cpu, data, size):
    event = b["events"].event(data)
    print(f"PID: {event.pid}, Time: {event.ts}, process name: {event.comm.decode()}")


b["events"].open_perf_buffer(print_event)
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
