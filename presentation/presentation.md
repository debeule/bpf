% Building an L4 load balancer on eBPF
% Matthieu De Beule
% 10.06.2020


# L4 load balancer with eBPF

Load balancers are an important part of any scaling web service

![](../images/lb.png){ width=280px }

They enable spreading the load over multiple servers

# L4 load balancer with eBPF

Load balancing can be done at various layers, most commonly at L4 and L7.

* L4 load balancers operate on the TCP/IP level, adjusting IPs and TCP/UDP ports. They do not tend to have any view of actual content in the packets.

* L7 load balancers are more CPU-intensive but can make very informed decisions since they operate at the application layer (e.g. HTTP).

# L4 load balancer with eBPF

With eBPF, we can make a performant load balancer that avoids unnecessary copies and can retransmit packets even before they go through the Linux stack, and perform other operations on the packets all without needing to allocate kernel structs and buffers to them:

![](../images/xdp.png){ width=350px }


# What is eBPF?

* A generic kernel execution engine

* A safe and efficient way to instrument the kernel

* Filter, modify, retransmit packets before the kernel network stack

# A generic kernel execution engine

* Virtual machine running inside the kernel, running eBPF assembly.
<!-- Here, talk about the mapping to native assembly -->

* Not Turing complete: only DAGs are valid eBPF programs
    * Verifier maintains the guarantees of the eBPF VM, only programs that pass it get executed
    * Bounded loops are now valid eBPF programs
    * Instruction limit was raised from 4K to 1M instructions

* Security guarantees: can do things previously only possible through a kernel module, without possibility of kernel panics, or unbounded loops causing hangs, etc.

# A generic kernel execution engine

eBPF programs can be hooked onto a lot of events. The eBPF program is executed every time this event is triggered, like on reception of a packet, or attached to kprobes, uprobes, tracepoints.

eBPF is often used for tracing (e.g. with kprobes, tracepoints) and for networking (e.g. with XDP).
![](../images/bpf_diagram.png)

# A generic kernel execution engine
## eBPF Maps

Maps offer generic storage of different types for sharing data between kernel and userspace. This allows for unbounded storage accessible from within eBPF and from userspace.

* Different eBPF programs and userspace can access the same map in parallel
* several types of maps are available, from arrays to associative maps to special maps useful for generating statistics (like histograms or stack traces)

# A safe and efficient way to instrument the kernel

1. Overview

2. kprobes and tracepoints

3. Tooling and how to use eBPF for tracing

# A safe and efficient way to instrument the kernel

## 1. Overview \newline

* Before eBPF, to instrument the kernel one would need to:  
instrument $\rightarrow$ recompile $\rightarrow$ install  $\rightarrow$  reboot

* With eBPF, we can attach a eBPF program anywhere to a running kernel, without fear of significant performance, stability, or security issues. \newline

For example, every Netflix server runs several eBPF programs, and more are often added to servers while they are running in production to document performance issues.

# A safe and efficient way to instrument the kernel

## 2. kprobes and tracepoints
### kprobes

* Very powerful, can be attached to any function without any changes necessary in the kernel.

* Not a stable API, so a eBPF program written for a version of the kernel should only be thought of as instrumenting that particular kernel.

###  tracepoints

* Not available everywhere, but *is* a stable API.

* Should be used instead of kprobes whenever available.

* Needs to be added explicitly to the kernel, and maintained.

* More and more of these available, since eBPF is driving demand for them.

# A safe and efficient way to instrument the kernel

## 3. Tooling and how to use eBPF for tracing \newline

There are various ways to use eBPF, going from bpftrace all the way to eBPF assembly, through BCC and the `bpf` syscall with `libbpf`. \newline

BCC is particularly easy to use, in conjunction with a python userspace component with its python bindings. \newline

bpftrace can make very powerful one-liners to quickly debug something, it's a high-level language specifically for eBPF tracing.\newline

This counts `read()` syscall errors by error code for example:
`bpftrace -e 't:syscalls:sys_exit_read /args->ret < 0/ { @[- args->ret] = count(); }'`


# A safe and efficient way to instrument the kernel

Example of instrumentation with BCC: logging when SYN packets are sent.

```{include=../code/detect_syn.py startLine=12 endLine=23 }
```

This will attach to the `tcp_connect` function in the TCP implementation of the kernel, so every time a connection is started it will be submitted to userspace.

# Filter, modify, retransmit packets before the kernel stack

Using XDP (eXpress Data Path) we can process packets very early in the network path, before the majority of processing by the kernel is done.

To use XDP, one should write a eBPF program and attach it to the XDP hook, where it must return what action is to be taken on the packet.

The eBPF program is run for each packet, where decisions can then be made

# Filter, modify, retransmit packets before the kernel stack

The XDP program's return value determines what will be done with the packet:

* `XDP_DROP` or `XDP_ABORTED` to drop
* `XDP_PASS` to pass it on (to the kernel network stack)
* `XDP_REDIRECT` to forward to another interface
* `XDP_TX` to transmit the packet back out from the same interface

XDP eBPF programs can also write to the packets and modify them as needed. However, as the XDP hook happens before the kernel network stack, the eBPF program has only pointers to the begin and end of the packet to work with. Manual bound checking and unwrapping of layers is needed to get to the UDP or TCP layer and access their fields.

# Typical XDP eBPF program:

![](../images/2020-06-08_23-50.png)

# L4 load balancer with eBPF

We can implement a load balancer by rewriting destination MAC and IP adresses in the packet, and then returning `XDP_TX`.

![](../images/xdp.png){ width=350px }  
The servers must also re-write the outgoing packets to have the IP of the balancer, which can be done with tc (**t**raffic **c**ontrol), also using eBPF.
<!-- Maybe quickly mention advantages/inconveniences of tc vs XDP -->


# L4 load balancer with eBPF

This way of implementing a load balancer is called **DSR** (Direct Server Return). DSR has the servers respond directly to the clients using the load balancer server's IP and MAC address.

![](../images/dsr.png)

This is as opposed to NAT (Network Address Translation) where returning packets also come through the load balancer server:

![](../images/nat.png)

# L4 load balancer with eBPF

* The IP/MAC address pairings of the servers are configured and accessed using eBPF maps: a userspace program fills in the maps when attaching the eBPF program.

* XDP can be very fast. For the use-case of rewriting IP and TCP or UDP headers for example as is the case for our load balancer, one could expect it to be able to handle around 10 million packets per second. eBPF programs can often be run on the NIC itself (hardware offloading is possible with more and more high-performance NICs)

# To resume

* eBPF is a very powerful tool, for:
    * tracing and instrumenting the kernel (kprobes, tracepoints)
    * networking (filters, load balancing)
    * more generally, doing most things a kernel module would be needed for, but safely and with guarantees.
